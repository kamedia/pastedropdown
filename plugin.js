CKEDITOR.plugins.add( 'pastedropdown', {
  icons: 'pastedropdown',
  init: function( editor ) {
    if ( editor.blockless )
    return;

    editor.addMenuGroup('paste_group');

    items = {};
    items.paste = {
      label: editor.lang.clipboard.paste,
      group: 'paste_group',
      command: 'paste',
      order: 1
    };
    items.pastetext = {
      label: editor.lang.pastetext.button,
      group: 'paste_group',
      command: 'pastetext',
      order: 2
    };
    items.pastefromword = {
      label: editor.lang.pastefromword.toolbar,
      group: 'paste_group',
      command: 'pastefromword',
      order: 3
    };

    if (editor.addMenuItems) {
      editor.addMenuItems( items );
    }

    editor.ui.addButton('PasteDropDown', {
        label: 'PasteDropDown',
        command: 'paste',
    });

    editor.ui.add('PasteDropDown', CKEDITOR.UI_MENUBUTTON, {
      label: editor.lang.clipboard.paste,
      icon: 'Paste',
      modes: {
        wysiwyg: 1
      },
      onMenu: function() {
        var active = {};
        // Make all items active.
        for ( var p in items )
          active[ p ] = CKEDITOR.TRISTATE_OFF;
        return active;
      }
    });
  }
});
